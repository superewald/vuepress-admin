import git from 'isomorphic-git'
import http from 'isomorphic-git/http'

export default function VuePressManager(opts) {
    this.settings = {...{
        fs: null,
        repository: "blrevive/wiki",
        branch: 'master',
        accessToken: '',
        userName: '',
    }, opts}
    this.settings.repoDir = '/' + repository + '/'
    this.fs = settings.fs
    this.pfs = settings.fs.promises


    function initialize() {
        await pfs.mkdir(this.repository)
        await git.clone({
            fs, http, repository,
            corsProxy: 'https://cors.isomorphic-git.org',
            url: 'https://gitlab.com/' + repository + '.git',
            ref: branch,
            singleBranch: true
        })
    }

    function getFiles() {}

    function getContent(file) {
        return await pfs.readFile(repoDir + file)
    }

    function updateContent(file, content) {
        return await pfs.writeFile(repoDir + file, content)
    }

    function getNavigation() {
        let vuepressConfig = new Function(getContent('.vuepress/config.js').replace('module.exports = ', 'return'))()
        return vuepressConfig['themeConfig']['nav']
    }

    function setNavigation(nav) {
        let vuepressConfig = new Function(getContent('.vuepress/config.js').replace('module.exports = ', 'return'))()
        vuepressConfig['themeConfig']['nav'] = nav
    }

    function saveChanges(message) {
        await git.add({fs, dir: repository, filepath: '.'})
        let sha = await git.commit({
            fs, dir: repository,
            author: { name: userName },
            message: message
        })
        return sha
    }   

    function submitChanges(message) {
        let res = git.push({
            fs, http, dir: repository, remote: 'origin', ref: branch,
            onAuth: () => ({username: accessToken})
        })

        if(res.ok) {
            return true
        }
        
        return false
    }
}